const user = require('./../../db/Schma');
const Ad = require('./../../db/Schma');


function fileUnlink(file, callback){

    fs.exists(file, function(exist){
        if (exist){
            fs.unlink(file, function(err){
                if (err){
                    callback('Could not remove file');
                } else {
                    callback(null, 'Success');
                }
            })
        } else {
            callback(null, 'Success');
        }
    });
}


function saveFile(saveDir, fileToUpload, alias, callback){

    var ext = fileToUpload.originalname.substr(fileToUpload.originalname.lastIndexOf('.'));
    var rootDir = '../files/';
    var rootLink = '/files/';

    if (saveDir.indexOf('\\') != -1){
        saveDir.replace(/\\/g, '/');
    }

    if (saveDir[0] == '/'){
        saveDir = saveDir.substr(1, saveDir.length-1);
    }
    if (saveDir[saveDir.length - 1] != '/'){
        saveDir = saveDir + '/';
    }
    var dirArray = saveDir.split('/');

    var fileAlias = fileToUpload.originalname;
    if (alias && (alias != '')) {
        if (alias.indexOf('.') != -1){
            fileAlias = alias;
        } else {
            fileAlias = alias + ext;
        }
    }
    var fileName = rootDir + saveDir + fileAlias;
    var fileLink = rootLink + saveDir + fileAlias;

    var dCounter = 0;
    for (var d=0; d<dirArray.length; d++){

        if (dirArray[d] != ''){

            var subDir = dirArray[d];
            if (d == 0){
                subDir = rootDir + subDir;
            } else {
                var tail = '';
                for (var sd=d; sd>0; sd--){
                    tail = dirArray[sd-1] + '/' + tail;
                }
                subDir = rootDir + tail + subDir;
            }

            checkDir(subDir, function(err){
                if (err){
                    callback('Could not create directory!')
                } else {

                    dCounter ++;
                    if (dCounter == dirArray.length){
                        saveImg(fileName, function(err){
                            if (err){
                                callback(err);
                            } else {
                                callback(null, fileLink);
                            }
                        });
                    }
                }
            });
        } else {

            dCounter ++;
            if (dCounter == dirArray.length){
                saveImg(fileName, function(err){
                    if (err){
                        callback(err);
                    } else {
                        callback(null, fileLink);
                    }
                });
            }
        }
    }

    function checkDir(dir, callback){

        if(!fs.existsSync(dir)){

            fs.mkdir(dir, function(err){
                if (err){
                    callback('Could not make dir');
                } else {
                    callback(null);
                }
            })
        } else {
            callback(null);
        }
    }

    function saveImg(fileName, callback){

        var fileBuffer = fs.readFileSync(fileToUpload.path);

        fs.writeFile(fileName, fileBuffer, function(err){
            if (err){
                console.log('------ Error: ' + err);
                callback('Could not upload file');
            } else {
                fileUnlink(fileToUpload.path, function(err){
                    if (err){
                        callback(err);
                    } else {
                        callback(null);
                    }
                });
            }
        })
    }
}

module.exports=function (req, res){ 

    var contact = [];
    for (var i in req.body.contacts) {
        contact.push(req.body.contacts[i]);
    };

    var newAd = new Ad({ // добавление нового объявления
        email: req.body.email,
        title: req.body.title,
        text: req.body.text, 
        timeLost: req.body.timeLost,
        placeLost: req.body.placeLost,
        contacts: contact,
        type: req.body.type
    });
    
    newAd.save(function(err) {
        if (err) {// вывод сообщения об ошибке
            const err_message = require('./../errorOutput')(Object.keys(err.errors)[0], Object.values(err.errors)[0].message);
            res.status(200).send(err_message);
        }
        else 
            res.send('Запись "'+ req.body.title + '" была успешно создана!');
    }); 
}
        