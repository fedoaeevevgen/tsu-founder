const express = require('express');
const bodyParser = require('body-parser');
var http = require('http');
var compression = require('compression');
var multer = require('multer');
var mongoose = require('mongoose');
var path = require('path');
var config = require('./libs/config');
const cookieParser = require('cookie-parser');//не факт что нужен из конечной сборки возможно надо удалить 
const cron = require("node-cron"); // для цикла по времени

mongoose.connect('mongodb://127.0.0.1/superblog', { useNewUrlParser: true , useCreateIndex: true }, function (err) {
if (err) 
    return console.log(err);

const app = express();

//cron.schedule("* * * * *", function() { // вызов проверки коллекции каждую минуту
cron.schedule("0 0 * * *", function() { // вызов проверки коллекции 1 раз в день
    require('./routes/addToArchive')(app);
});

app.use(compression());

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.use(multer(
    {
        dest: path.join(__dirname, 'public/uploads'),
        limits: {
            fieldNameSize: 999999999,
            fieldSize: 999999999
        }
    }
).any());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});
app.use('/files', express.static('../files'));

//************************* Routes ***********************************
require('./routes')(app);
//************************* 404 ***********************************
app.use(function(req, res){
    res.send('404 ошибка');
});
//************************* Запуск сервера ***********************************
// var httpServer = http.createServer(app);
// function onListening(){
//     console.log('Listening on port ', config.port);
// }
// httpServer.on('listening', onListening);
// httpServer.listen(config.port, 'localhost');
app.listen(config.port);
    console.log('Listening on port ', config.port);
});